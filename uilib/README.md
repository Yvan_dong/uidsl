# UILib

This is the standard library of UIDSL.

### UI Widgets

- [x] Label
- [x] Either
- [ ] List  
...

### Usage

Run `npm install` first.

Write the following code in `main.ts`

```typescript
import { UI } from "./src/UI";
import { TestLabel } from "./test/TestLabel"
import { TestEither } from "./test/TestEither"
import * as _ from 'lodash';

let testLabel = new TestLabel();

let body = document.createElement("body");
document.body = body;

testLabel.UiGeneActionWithDom(document.body);

let testEither = new TestEither();
testEither.UiGeneActionWithDom(document.body);
```

Run `npm build`. Then the webpack whould generate corresponding javascript file(main.js) in `dist` directory.

Then you can include it in HTML file just like the `index.html` in `dist` directory.

![demo1](./pic/demo1.png)
