/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./main.ts":
/*!*****************!*\
  !*** ./main.ts ***!
  \*****************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _test_TestLabel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./test/TestLabel */ \"./test/TestLabel.ts\");\n/* harmony import */ var _test_TestEither__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./test/TestEither */ \"./test/TestEither.ts\");\n\n\nvar testLabel = new _test_TestLabel__WEBPACK_IMPORTED_MODULE_0__.TestLabel();\nvar body = document.createElement(\"body\");\ndocument.body = body;\ntestLabel.UiGeneActionWithDom(document.body);\nvar testEither = new _test_TestEither__WEBPACK_IMPORTED_MODULE_1__.TestEither();\ntestEither.UiGeneActionWithDom(document.body);\n\n\n//# sourceURL=webpack://uilib/./main.ts?");

/***/ }),

/***/ "./src/BaseObject.ts":
/*!***************************!*\
  !*** ./src/BaseObject.ts ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"BaseObject\": () => (/* binding */ BaseObject)\n/* harmony export */ });\n// base type of state object\nvar BaseObject = /** @class */ (function () {\n    function BaseObject() {\n    }\n    return BaseObject;\n}());\n\n\n\n//# sourceURL=webpack://uilib/./src/BaseObject.ts?");

/***/ }),

/***/ "./src/Either.ts":
/*!***********************!*\
  !*** ./src/Either.ts ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Condition\": () => (/* binding */ Condition),\n/* harmony export */   \"Either\": () => (/* binding */ Either)\n/* harmony export */ });\n/* harmony import */ var _StateObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StateObject */ \"./src/StateObject.ts\");\n/* harmony import */ var _ListenerObject__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListenerObject */ \"./src/ListenerObject.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\n    var extendStatics = function (d, b) {\n        extendStatics = Object.setPrototypeOf ||\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\n        return extendStatics(d, b);\n    };\n    return function (d, b) {\n        extendStatics(d, b);\n        function __() { this.constructor = d; }\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\n    };\n})();\n\n\n// condition of if/else view\n// this is not a UI, just a state-listener\n// Listen to the StateObjects in the condition expression\nvar Condition = /** @class */ (function (_super) {\n    __extends(Condition, _super);\n    function Condition(value) {\n        var _this = _super.call(this) || this;\n        // init the condition value\n        if (value != null) {\n            _this._value = new _StateObject__WEBPACK_IMPORTED_MODULE_0__.StateObject(value);\n        }\n        else {\n            _this._value = new _StateObject__WEBPACK_IMPORTED_MODULE_0__.StateObject(false);\n        }\n        return _this;\n    }\n    Condition.prototype.handler = function (stateObject) {\n        this.update(this, stateObject);\n        if (this._need_regenerate) {\n            this.UiGeneAction();\n        }\n    };\n    Condition.prototype.UiGeneAction = function () {\n        throw new Error(\"Should not reach here.\");\n    };\n    Condition.prototype.AddListener = function (listener) {\n        this._value.AddListener(listener);\n    };\n    // return state value\n    Condition.prototype.value = function () { return this._value.value(); };\n    // update state value\n    Condition.prototype.setValue = function (val) {\n        this._value.setValue(val);\n    };\n    return Condition;\n}(_ListenerObject__WEBPACK_IMPORTED_MODULE_1__.ListenerObject));\n\n// ui for if/else widget\nvar Either = /** @class */ (function (_super) {\n    __extends(Either, _super);\n    function Either(cond, then, else_) {\n        var _this = _super.call(this) || this;\n        _this._condition = cond;\n        _this._then_view = then;\n        _this._else_view = else_;\n        // set flags\n        _this._then_view.SetInEither(true);\n        _this._else_view.SetInEither(true);\n        // add Either to condition's listeners\n        _this._condition.AddListener(_this);\n        return _this;\n    }\n    Either.prototype.handler = function (stateObject) {\n        // update view if method exist\n        if (this.update != null)\n            this.update(this);\n        // regenerate ui\n        this.UiGeneAction();\n    };\n    // generate branch ui\n    Either.prototype.BranchGene = function () {\n        var node;\n        if (this._condition.value()) {\n            this._then_view.SetNeedShow(true);\n            this._else_view.SetNeedShow(false);\n            node = this._then_view.UiGeneAction();\n        }\n        else {\n            this._then_view.SetNeedShow(false);\n            this._else_view.SetNeedShow(true);\n            node = this._else_view.UiGeneAction();\n        }\n        return node;\n    };\n    Either.prototype.UiGeneAction = function () {\n        // perform init check\n        this.InitCheck();\n        // check if it is either's component\n        if (this._in_either) {\n            if (this._need_show == false) {\n                return null;\n            }\n        }\n        var node;\n        // genereate ui according condtion\n        this._then_view.SetParent(this._parent);\n        this._else_view.SetParent(this._parent);\n        node = this.BranchGene();\n        // set view\n        this._view = node;\n        // add to parent dom node\n        this._parent.appendChild(this._view);\n        return this._view;\n    };\n    return Either;\n}(_ListenerObject__WEBPACK_IMPORTED_MODULE_1__.ListenerObject));\n\n\n\n//# sourceURL=webpack://uilib/./src/Either.ts?");

/***/ }),

/***/ "./src/Label.ts":
/*!**********************!*\
  !*** ./src/Label.ts ***!
  \**********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"Label\": () => (/* binding */ Label)\n/* harmony export */ });\n/* harmony import */ var _ListenerObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ListenerObject */ \"./src/ListenerObject.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\n    var extendStatics = function (d, b) {\n        extendStatics = Object.setPrototypeOf ||\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\n        return extendStatics(d, b);\n    };\n    return function (d, b) {\n        extendStatics(d, b);\n        function __() { this.constructor = d; }\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\n    };\n})();\n\n// html label type\nvar Label = /** @class */ (function (_super) {\n    __extends(Label, _super);\n    function Label() {\n        return _super !== null && _super.apply(this, arguments) || this;\n    }\n    Label.prototype.handler = function (stateObject) {\n        // update view if method exist\n        if (this.update != null)\n            this.update(this);\n        // regenerate ui\n        if (this._need_regenerate) {\n            this.UiGeneAction();\n        }\n    };\n    /* get value */\n    Label.prototype.value = function () { return this._value; };\n    /* set value*/\n    Label.prototype.setValue = function (value) {\n        this._value = value;\n        if (this._view != null) {\n            this._view.innerText = this._value;\n        }\n    };\n    /* get for name */\n    Label.prototype.forname = function () { return this._forname; };\n    /* set for name */\n    Label.prototype.setForname = function (forname) { this._forname = forname; };\n    /* Generate ui in parent dom tree */\n    Label.prototype.UiGeneAction = function () {\n        // perform init check\n        this.InitCheck();\n        // check if it is either's component\n        if (this._in_either) {\n            if (this._need_show == false) {\n                return null;\n            }\n        }\n        // regenerate view\n        var label = document.createElement(\"label\");\n        // set inner text\n        label.innerText = this._value;\n        // set attribute\n        label.setAttribute(\"for\", this._forname);\n        this._view = label;\n        // add to parent dom node\n        this._parent.appendChild(this._view);\n        return this._view;\n    };\n    return Label;\n}(_ListenerObject__WEBPACK_IMPORTED_MODULE_0__.ListenerObject));\n\n\n\n//# sourceURL=webpack://uilib/./src/Label.ts?");

/***/ }),

/***/ "./src/ListenerObject.ts":
/*!*******************************!*\
  !*** ./src/ListenerObject.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"ListenerObject\": () => (/* binding */ ListenerObject)\n/* harmony export */ });\n/* harmony import */ var _UI__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./UI */ \"./src/UI.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\n    var extendStatics = function (d, b) {\n        extendStatics = Object.setPrototypeOf ||\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\n        return extendStatics(d, b);\n    };\n    return function (d, b) {\n        extendStatics(d, b);\n        function __() { this.constructor = d; }\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\n    };\n})();\n\n// listener\nvar ListenerObject = /** @class */ (function (_super) {\n    __extends(ListenerObject, _super);\n    function ListenerObject() {\n        var _this = \n        // call super constructor\n        _super.call(this) || this;\n        // init state objects list\n        _this._stateObjects = new Array();\n        return _this;\n    }\n    /* Receive message of changed state value */\n    /* invoke this method as its value getting changed */\n    ListenerObject.prototype.RecvMessage = function (stateObject) {\n        this.handler(stateObject);\n    };\n    /* Add State Object into array */\n    ListenerObject.prototype.AddStateObject = function (stateObject) {\n        this._stateObjects.push(stateObject);\n    };\n    /* set update function */\n    ListenerObject.prototype.setUpdate = function (method) { this.update = method; };\n    return ListenerObject;\n}(_UI__WEBPACK_IMPORTED_MODULE_0__.UI));\n\n\n\n//# sourceURL=webpack://uilib/./src/ListenerObject.ts?");

/***/ }),

/***/ "./src/StateObject.ts":
/*!****************************!*\
  !*** ./src/StateObject.ts ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"StateObject\": () => (/* binding */ StateObject)\n/* harmony export */ });\n/* harmony import */ var _BaseObject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseObject */ \"./src/BaseObject.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\n    var extendStatics = function (d, b) {\n        extendStatics = Object.setPrototypeOf ||\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\n        return extendStatics(d, b);\n    };\n    return function (d, b) {\n        extendStatics(d, b);\n        function __() { this.constructor = d; }\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\n    };\n})();\n\n// state value wrapper\n// broadcast to its listeners as its value being changed\nvar StateObject = /** @class */ (function (_super) {\n    __extends(StateObject, _super);\n    function StateObject(value) {\n        var _this = \n        // constructor of super class\n        _super.call(this) || this;\n        // store the state value\n        _this._value = value;\n        _this._listeners = new Array();\n        return _this;\n    }\n    /* inform listener */\n    StateObject.prototype.informLinstener = function (listener) {\n        listener.RecvMessage(this);\n    };\n    /* broadcast listeners */\n    StateObject.prototype.broadcast = function () {\n        for (var _i = 0, _a = this._listeners; _i < _a.length; _i++) {\n            var it = _a[_i];\n            this.informLinstener(it);\n        }\n    };\n    /* value getter */\n    StateObject.prototype.value = function () { return this._value; };\n    StateObject.prototype.valueOf = function () { return this.value(); };\n    /* add listener */\n    StateObject.prototype.AddListener = function (listener) {\n        this._listeners.push(listener);\n        // add itself into listener's list\n        listener.AddStateObject(this);\n    };\n    /* update value */\n    StateObject.prototype.setValue = function (value) {\n        // return if not changed\n        if (value == this._value)\n            return;\n        // update state value\n        this._value = value;\n        // inform listeners \n        this.broadcast();\n    };\n    StateObject.prototype.set = function (value) {\n        this.setValue(value);\n    };\n    return StateObject;\n}(_BaseObject__WEBPACK_IMPORTED_MODULE_0__.BaseObject));\n\n;\n\n\n//# sourceURL=webpack://uilib/./src/StateObject.ts?");

/***/ }),

/***/ "./src/UI.ts":
/*!*******************!*\
  !*** ./src/UI.ts ***!
  \*******************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"UI\": () => (/* binding */ UI)\n/* harmony export */ });\n// basic of ui\nvar UI = /** @class */ (function () {\n    function UI() {\n        this._in_either = false;\n        this._need_show = false;\n        this._need_regenerate = false;\n    }\n    UI.prototype.InitCheck = function () {\n        // check if parent dom node exist\n        if (this._parent == null) {\n            throw new Error(\"Parent dom node not exist\");\n        }\n        // remove exist view\n        if (this._view != null) {\n            if (this._parent.contains(this._view)) {\n                this._parent.removeChild(this._view);\n            }\n            this._view = null;\n        }\n    };\n    // set need regenerate\n    UI.prototype.SetNeedRegenerate = function (val) {\n        this._need_regenerate = val;\n    };\n    // set parent dom of this view\n    UI.prototype.SetParent = function (parent) {\n        this._parent = parent;\n    };\n    // set _in_either value\n    UI.prototype.SetInEither = function (val) { this._in_either = val; };\n    // set _need_show\n    UI.prototype.SetNeedShow = function (val) { this._need_show = val; };\n    // check if in either\n    UI.prototype.InEither = function () { return this._in_either; };\n    // check if need show\n    UI.prototype.NeedShow = function () { return this._need_show; };\n    UI.prototype.UiGeneActionWithDom = function (dom) {\n        this._parent = dom;\n        return this.UiGeneAction();\n    };\n    return UI;\n}());\n\n;\n\n\n//# sourceURL=webpack://uilib/./src/UI.ts?");

/***/ }),

/***/ "./test/TestEither.ts":
/*!****************************!*\
  !*** ./test/TestEither.ts ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"TestEither\": () => (/* binding */ TestEither)\n/* harmony export */ });\n/* harmony import */ var _src_UI__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../src/UI */ \"./src/UI.ts\");\n/* harmony import */ var _src_Label__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../src/Label */ \"./src/Label.ts\");\n/* harmony import */ var _src_StateObject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../src/StateObject */ \"./src/StateObject.ts\");\n/* harmony import */ var _src_Either__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../src/Either */ \"./src/Either.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\n    var extendStatics = function (d, b) {\n        extendStatics = Object.setPrototypeOf ||\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\n        return extendStatics(d, b);\n    };\n    return function (d, b) {\n        extendStatics(d, b);\n        function __() { this.constructor = d; }\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\n    };\n})();\n\n\n\n\nvar TestEither = /** @class */ (function (_super) {\n    __extends(TestEither, _super);\n    function TestEither() {\n        var _this = _super.call(this) || this;\n        _this._cond = new _src_Either__WEBPACK_IMPORTED_MODULE_3__.Condition(false);\n        _this._then = new _src_Label__WEBPACK_IMPORTED_MODULE_1__.Label();\n        _this._else = new _src_Label__WEBPACK_IMPORTED_MODULE_1__.Label();\n        // init state-listen relationship\n        _this._cond.AddListener(_this._then);\n        _this._cond.AddListener(_this._else);\n        // set then update function\n        _this._then.setUpdate(function (label) {\n            label.setValue(\"then\" + _this._cond.value());\n        });\n        // set else update function\n        _this._else.setUpdate(function (label) {\n            label.setValue(\"else\" + _this._cond.value());\n        });\n        // init cond operand\n        _this._op1 = new _src_StateObject__WEBPACK_IMPORTED_MODULE_2__.StateObject(10);\n        _this._op1.AddListener(_this._cond);\n        // set condition update after the initialization of operands\n        _this._cond.setUpdate(function (condition) {\n            condition.setValue(_this._op1.value() - 5 > 0);\n        });\n        // init Either object\n        _this._either = new _src_Either__WEBPACK_IMPORTED_MODULE_3__.Either(_this._cond, _this._then, _this._else);\n        return _this;\n        // set update method of Either(optional)\n    }\n    TestEither.prototype.UiGeneAction = function () {\n        var _this = this;\n        this.InitCheck();\n        // regenerate view\n        var div = document.createElement(\"div\");\n        this._view = div;\n        // add to parent dom node\n        this._parent.appendChild(this._view);\n        // generate either\n        this._either.UiGeneActionWithDom(this._view);\n        // genereate button\n        var button = document.createElement(\"button\");\n        button.innerHTML = \"Modify Either\";\n        var _bt_clk = function () {\n            var _op1_value = _this._op1.value() - 1;\n            _this._op1.setValue(_op1_value);\n        };\n        button.onclick = _bt_clk;\n        this._parent.appendChild(button);\n        return this._view;\n    };\n    return TestEither;\n}(_src_UI__WEBPACK_IMPORTED_MODULE_0__.UI));\n\n\n\n//# sourceURL=webpack://uilib/./test/TestEither.ts?");

/***/ }),

/***/ "./test/TestLabel.ts":
/*!***************************!*\
  !*** ./test/TestLabel.ts ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"TestLabel\": () => (/* binding */ TestLabel)\n/* harmony export */ });\n/* harmony import */ var _src_UI__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../src/UI */ \"./src/UI.ts\");\n/* harmony import */ var _src_Label__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../src/Label */ \"./src/Label.ts\");\n/* harmony import */ var _src_StateObject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../src/StateObject */ \"./src/StateObject.ts\");\nvar __extends = (undefined && undefined.__extends) || (function () {\n    var extendStatics = function (d, b) {\n        extendStatics = Object.setPrototypeOf ||\n            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||\n            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };\n        return extendStatics(d, b);\n    };\n    return function (d, b) {\n        extendStatics(d, b);\n        function __() { this.constructor = d; }\n        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());\n    };\n})();\n\n\n\nvar TestLabel = /** @class */ (function (_super) {\n    __extends(TestLabel, _super);\n    function TestLabel() {\n        var _this = _super.call(this) || this;\n        _this._id = new _src_StateObject__WEBPACK_IMPORTED_MODULE_2__.StateObject(0);\n        _this._label = new _src_Label__WEBPACK_IMPORTED_MODULE_1__.Label();\n        // init state-listen relationship\n        _this._id.AddListener(_this._label);\n        var update = function (label) {\n            label.setValue(\"label\" + _this._id.value());\n        };\n        _this._label.setUpdate(update);\n        return _this;\n    }\n    TestLabel.prototype.UiGeneAction = function () {\n        var _this = this;\n        this.InitCheck();\n        // regenerate view\n        var div = document.createElement(\"div\");\n        this._view = div;\n        // add to parent dom node\n        this._parent.appendChild(this._view);\n        // genereate label\n        this._label.UiGeneActionWithDom(this._view);\n        // generate button\n        var button = document.createElement(\"button\");\n        button.innerHTML = \"Modify Label\";\n        button.onclick = function () { _this._id.setValue(_this._id.value() + 1); };\n        this._parent.appendChild(button);\n        return div;\n    };\n    return TestLabel;\n}(_src_UI__WEBPACK_IMPORTED_MODULE_0__.UI));\n\n\n\n//# sourceURL=webpack://uilib/./test/TestLabel.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./main.ts");
/******/ 	
/******/ })()
;