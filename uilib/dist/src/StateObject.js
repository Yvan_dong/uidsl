var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { BaseObject } from "./BaseObject";
// state value wrapper
// broadcast to its listeners as its value being changed
var StateObject = /** @class */ (function (_super) {
    __extends(StateObject, _super);
    function StateObject(value) {
        var _this = 
        // constructor of super class
        _super.call(this) || this;
        // store the state value
        _this._value = value;
        return _this;
    }
    /* inform listener */
    StateObject.prototype.informLinstener = function (listener) {
        listener.RecvMessage(this);
    };
    /* broadcast listeners */
    StateObject.prototype.broadcast = function () {
        for (var _i = 0, _a = this._listeners; _i < _a.length; _i++) {
            var it = _a[_i];
            this.informLinstener(it);
        }
    };
    /* value getter */
    StateObject.prototype.value = function () { return this._value; };
    /* add listener */
    StateObject.prototype.AddListener = function (listener) {
        this._listeners.push(listener);
        // add itself into listener's list
        listener.AddStateObject(this);
    };
    /* update value */
    StateObject.prototype.setValue = function (value) {
        // return if not changed
        if (value == this._value)
            return;
        // update state value
        this._value = value;
        // inform listeners 
        this.broadcast();
    };
    return StateObject;
}(BaseObject));
export { StateObject };
;
