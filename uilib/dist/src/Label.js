var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { ListenerObject } from "./ListenerObject";
// html label type
var Label = /** @class */ (function (_super) {
    __extends(Label, _super);
    function Label() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Label.prototype.handler = function (stateObject) {
        this.update();
        this.UiGeneAction();
    };
    /* get value */
    Label.prototype.value = function () { return this._value; };
    /* set value*/
    Label.prototype.setValue = function (value) { this._value = value; };
    /* get for name */
    Label.prototype.forname = function () { return this._forname; };
    /* set for name */
    Label.prototype.setForname = function (forname) { this._forname = forname; };
    /* set update function */
    Label.prototype.setUpdate = function (method) { this.update = method; };
    /* Generate ui in parent dom tree */
    Label.prototype.UiGeneAction = function () {
        // perform init check
        this.InitCheck();
        // regenerate view
        var label = document.createElement("label");
        // set inner text
        label.innerText = this._value;
        // set attribute
        label.setAttribute("for", this._forname);
        this._view = label;
        // add to parent dom node
        this._parent.appendChild(this._view);
    };
    return Label;
}(ListenerObject));
export { Label };
