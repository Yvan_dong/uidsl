var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { UI } from "./UI";
// listener
var ListenerObject = /** @class */ (function (_super) {
    __extends(ListenerObject, _super);
    function ListenerObject() {
        return _super.call(this) || this;
    }
    /* Receive message of changed state value */
    /* invoke this method as its value getting changed */
    ListenerObject.prototype.RecvMessage = function (stateObject) {
        this.handler(stateObject);
    };
    /* Add State Object into array */
    ListenerObject.prototype.AddStateObject = function (stateObject) {
        this._stateObjects.push(stateObject);
    };
    return ListenerObject;
}(UI));
export { ListenerObject };
