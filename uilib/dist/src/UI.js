// basic of ui
var UI = /** @class */ (function () {
    function UI() {
    }
    UI.prototype.InitCheck = function () {
        // check if parent dom node exist
        if (this._parent == null) {
            throw new Error("Parent dom node not exist");
        }
        // remove exist view
        if (this._view != null) {
            this._parent.removeChild(this._view);
            this._view = null;
        }
    };
    UI.prototype.UiGeneActionWithDom = function (dom) {
        this._parent = dom;
        this.UiGeneAction();
    };
    return UI;
}());
export { UI };
;
