import { UI } from "../src/UI";
import { Label } from "../src/Label";
import { StateObject } from "../src/StateObject"
import { Condition, Either } from "../src/Either";

export class TestEither extends UI {
  private _id: number;
  private _cond: Condition;
  private _then: Label;
  private _else: Label;
  private _either: Either;

  // state value in condition
  private _op1: StateObject<number>;

  constructor() {
    super();
    this._cond = new Condition(false);
    this._then = new Label();
    this._else = new Label();

    // init state-listen relationship
    this._cond.AddListener(this._then);
    this._cond.AddListener(this._else);

    // set then update function
    this._then.setUpdate((label: Label): void => {
      label.setValue("then" + this._cond.value());
    });

    // set else update function
    this._else.setUpdate((label: Label): void => {
      label.setValue("else" + this._cond.value());
    });

    // init cond operand
    this._op1 = new StateObject<number>(10);
    this._op1.AddListener(this._cond);

    // set condition update after the initialization of operands
    this._cond.setUpdate((condition: Condition): void => {
      condition.setValue(this._op1.value() - 5 > 0);
    });

    // init Either object
    this._either = new Either(this._cond, this._then, this._else);

    // set update method of Either(optional)
  }

  public UiGeneAction() {
    this.InitCheck();

    // regenerate view
    let div = document.createElement("div");
    this._view = div;

    // add to parent dom node
    this._parent.appendChild(this._view);

    // generate either
    this._either.UiGeneActionWithDom(this._view);

    // genereate button
    let button = document.createElement("button");
    button.innerHTML = "Modify Either";
    let _bt_clk = (): void => {
      let _op1_value = this._op1.value() - 1;
      this._op1.setValue(_op1_value);
    }
    button.onclick = _bt_clk;
    this._parent.appendChild(button);

    return this._view;
  }
}