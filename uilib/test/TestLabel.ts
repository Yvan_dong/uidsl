import { UI } from "../src/UI";
import { Label } from "../src/Label";
import { StateObject } from "../src/StateObject"

export class TestLabel extends UI {
  public _id: StateObject<number>;
  private _label: Label;

  constructor() {
    super();
    this._id = new StateObject<number>(0);
    this._label = new Label();

    // init state-listen relationship
    this._id.AddListener(this._label);
    let update = (label: Label): void => {
      label.setValue("label" + this._id.value());
    }
    this._label.setUpdate(update);
  }

  public UiGeneAction(): Node {
    this.InitCheck();

    // regenerate view
    let div = document.createElement("div");
    this._view = div;

    // add to parent dom node
    this._parent.appendChild(this._view);

    // genereate label
    this._label.UiGeneActionWithDom(this._view);

    // generate button
    let button = document.createElement("button");
    button.innerHTML = "Modify Label";
    button.onclick = (): void => { this._id.setValue(this._id.value() + 1); }
    this._parent.appendChild(button);

    return div;
  }

}