import { TestLabel } from "./test/TestLabel"
import { TestEither } from "./test/TestEither"
import * as _ from 'lodash';

// init html root node
let body = document.createElement("body");
document.body = body;

// generate label tester
let testLabel = new TestLabel();
testLabel.UiGeneActionWithDom(document.body);

// generate either tester
let testEither = new TestEither();
testEither.UiGeneActionWithDom(document.body);