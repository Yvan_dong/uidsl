import { BaseObject } from "./BaseObject";
import { ListenerObject } from "./ListenerObject"

// state value wrapper
// broadcast to its listeners as its value being changed
export class StateObject<Type> extends BaseObject {
  /* the @state value in UI */
  protected _value: Type;

  /* listeners of the state value */
  protected _listeners: Array<ListenerObject>

  /* inform listener */
  protected informLinstener(listener: ListenerObject) {
    listener.RecvMessage(this);
  }

  /* broadcast listeners */
  public broadcast() {
    for (let it of this._listeners) {
      this.informLinstener(it);
    }
  }

  constructor(value: Type) {
    // constructor of super class
    super();

    // store the state value
    this._value = value;

    this._listeners = new Array<ListenerObject>();
  }

  /* value getter */
  public value() { return this._value; }
  public valueOf() { return this.value(); }

  /* add listener */
  public AddListener(listener: ListenerObject) {
    this._listeners.push(listener);

    // add itself into listener's list
    listener.AddStateObject(this);
  }

  /* update value */
  public setValue(value: Type) {
    // return if not changed
    if (value == this._value) return;

    // update state value
    this._value = value;

    // inform listeners 
    this.broadcast();
  }

  public set(value: Type) {
    this.setValue(value);
  }
};
