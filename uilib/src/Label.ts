import { BaseObject } from "./BaseObject"
import { ListenerObject } from "./ListenerObject"

// html label type
export class Label extends ListenerObject {
  // label string
  private _value: string;

  // label for
  private _forname: string;

  protected handler(stateObject: BaseObject) {
    // update view if method exist
    if (this.update != null) this.update(this);

    // regenerate ui
    if (this._need_regenerate) {
      this.UiGeneAction();
    }
  }

  /* get value */
  public value() { return this._value; }
  /* set value*/
  public setValue(value: string): void {
    this._value = value;
    if (this._view != null) {
      (<HTMLElement>this._view).innerText = this._value;
    }
  }

  /* get for name */
  public forname() { return this._forname; }
  /* set for name */
  public setForname(forname: string): void { this._forname = forname; }

  /* Generate ui in parent dom tree */
  public UiGeneAction(): Node {
    // perform init check
    this.InitCheck();

    // check if it is either's component
    if (this._in_either) {
      if (this._need_show == false) {
        return null;
      }
    }

    // regenerate view
    let label = document.createElement("label");

    // set inner text
    label.innerText = this._value;

    // set attribute
    label.setAttribute("for", this._forname);
    this._view = label;

    // add to parent dom node
    this._parent.appendChild(this._view);

    return this._view;
  }
}
