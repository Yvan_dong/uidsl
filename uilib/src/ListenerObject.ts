
import { UI } from "./UI"
import { BaseObject } from "./BaseObject"

// listener
export abstract class ListenerObject extends UI {
  protected _stateObjects: Array<BaseObject>;

  // this is a function
  protected update: any;

  constructor() {
    // call super constructor
    super();

    // init state objects list
    this._stateObjects = new Array<BaseObject>();
  }

  /* Handle changed widget */
  /* Widget should override this method */
  protected abstract handler(stateObject: BaseObject): void;

  /* Receive message of changed state value */
  /* invoke this method as its value getting changed */
  public RecvMessage(stateObject: BaseObject): void {
    this.handler(stateObject);
  }

  /* Add State Object into array */
  public AddStateObject(stateObject: BaseObject) {
    this._stateObjects.push(stateObject);
  }

  /* set update function */
  public setUpdate(method: any): void { this.update = method; }
}