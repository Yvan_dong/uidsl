import { BaseObject } from "./BaseObject"
import { StateObject } from "./StateObject"
import { ListenerObject } from "./ListenerObject";
import { UI } from "./UI";

// condition of if/else view
// this is not a UI, just a state-listener
// Listen to the StateObjects in the condition expression
export class Condition extends ListenerObject {
  // value the condition
  private _value: StateObject<boolean>

  constructor(value?: boolean) {
    super();

    // init the condition value
    if (value != null) {
      this._value = new StateObject<boolean>(value);
    } else {
      this._value = new StateObject<boolean>(false);
    }
  }

  protected handler(stateObject: BaseObject): void {
    this.update(this, stateObject);

    if (this._need_regenerate) {
      this.UiGeneAction();
    }
  }

  public UiGeneAction(): Node {
    throw new Error("Should not reach here.");
  }

  public AddListener(listener: ListenerObject) {
    this._value.AddListener(listener);
  }

  // return state value
  public value() { return this._value.value(); }

  // update state value
  public setValue(val: boolean) {
    this._value.setValue(val);
  }

}

// ui for if/else widget
export class Either extends ListenerObject {
  // state value of condition
  private _condition: Condition;

  // view for if widget
  private _then_view: UI;

  // view for else widget
  private _else_view: UI;

  constructor(cond: Condition, then: UI, else_: UI) {
    super();

    this._condition = cond;
    this._then_view = then;
    this._else_view = else_;

    // set flags
    this._then_view.SetInEither(true);
    this._else_view.SetInEither(true);

    // add Either to condition's listeners
    this._condition.AddListener(this);
  }

  public handler(stateObject: BaseObject): void {
    // update view if method exist
    if (this.update != null) this.update(this);

    // regenerate ui
    this.UiGeneAction();
  }

  // generate branch ui
  public BranchGene(): any {
    var node: any;
    if (this._condition.value()) {
      this._then_view.SetNeedShow(true);
      this._else_view.SetNeedShow(false);
      node = this._then_view.UiGeneAction();
    } else {
      this._then_view.SetNeedShow(false);
      this._else_view.SetNeedShow(true);
      node = this._else_view.UiGeneAction();
    }
    return node;
  }

  public UiGeneAction(): Node {
    // perform init check
    this.InitCheck();

    // check if it is either's component
    if (this._in_either) {
      if (this._need_show == false) {
        return null;
      }
    }

    var node: any;

    // genereate ui according condtion
    this._then_view.SetParent(this._parent);
    this._else_view.SetParent(this._parent);
    node = this.BranchGene();

    // set view
    this._view = node;

    // add to parent dom node
    this._parent.appendChild(this._view);

    return this._view;
  }

  /* set condition value */
}